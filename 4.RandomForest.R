#######################################################################################################################
# Random Forest 
# 
# Author:     Katie Gale
#             Katie.Gale@dfo-mpo.gc.ca
#             250-363-6411
#
# Date:       Dec 3, 2018
#
#
# Inputs: 
#   - site x species matrix with field for grouping (e.g., cluster). E.g., matFullCl from cluster analysis.   
#   - species lookup table (optional) - for adding species names to codes
#   - species (species list)
#   - legendcluster (list of "top clusters" from cluster analysis output)
#   - a stack of envionmental rasters for predictions
#
# Outputs and Saved intermediate files that are used as inputs in the indicator species and random forest analyses:
#   - For each cluster/group, list of indicator species above p-value and indval thresholds.
#     -- Also shows prevalence of those indicator species species in other clusters
#
######################################################################################################################

## Import libraries
library(randomForest)
library(raster)
library(rgdal)
library(reshape)
library(vegan)
library(ggplot2)
library(pROC)
library(corrplot)
library(caret)

#---------------------------#
# Set up parameters
#---------------------------#
#Set up working directory 
dir<-getwd()
outdir<-"2018.11.20_removeSpeciesGroupings/" # place the input cluster, environment, and species data here (outputs from cluster analysis)

# Is this a cluster-classification or single-species (pres/abs) model?
modelType<-"cluster" #one of "cluster" or "species" 

# If single species, what is the field name?
sp<-"A_ZO" #(e.g., A_ZO, I_RSC )

# label for output figure
figLabel<-"RemoveGroupings_2018.11.20"

#raster location
rasLoc<-"D:/Documents/!GIS Files/EnvironmentalRasters/Nearshore/Rasters"

#ID field
idField<-"ID"

#survey field
surveyField<-NA #if no survey field (i.e., covariate), set to NA

#do prediction?
predict<-"yes"

#---------------------------#
# Create subfolder if needed
#---------------------------#
if(modelType=="species"){
  outdir<- paste0(outdir,"/",sp,"/")
  dir.create( paste0("./",outdir,"/",sp,"/"), recursive = T)
}

#---------------------------#
# Bring in output from cluster analysis 
#---------------------------#
#Import site x species + env matrix, with assigned clusters from cluster analysis, from cluster analysis code
# Bare minimum is factor to be predicted (single species or cluster assignment) with environmental variables, by site/sample
if(modelType=="species"){
  matFullCl<-read.csv(paste0(gsub(sp,"",outdir),"matFullCl.csv")) 
  } else {
  matFullCl<-read.csv(paste0(outdir,"matFullCl.csv"))  
  }
matFullCl<-matFullCl[complete.cases(matFullCl),]

#---------------------------#
# Prep data
#---------------------------#
#Specify environmental variables (output from cluster analysis code)
env<-readRDS(paste0(gsub(sp,"",outdir),"envVar.rdata"))
env[env=="bathy.1"]<-"bathy" #name correction so it matches rasters

#Load rasters for prediction
setwd(rasLoc)
ras<-stack(paste0(env, ".tif"))
env[env=="bathy"]<-"bathy.1"
names(ras)[names(ras)=="bathy"]<-"bathy.1"
setwd(dir)

#Load legend (contains the clusters of interest) and prep data for model
if (modelType=="cluster"){
  legendcluster<-readRDS(paste0(outdir,"/legendCluster.rds"))
  forRF<-matFullCl[matFullCl$cl %in% legendcluster$cl,] # need to bring legend in to specify clusters
  forRF<-forRF[,names(forRF)%in% c(env,"cl","Survey")]
  forRF$cl<-as.factor(forRF$cl)
} else { 
  forRF<-matFullCl
  forRF<-forRF[,names(forRF)%in% c(env,sp,"Survey")]
  forRF[,sp]<-as.factor(as.character(forRF[,sp]))
}

forRF$Substrate<-as.factor(forRF$Substrate)
if(!is.na(surveyField)){
forRF[,surveyField]<-as.factor(forRF[,surveyField])}

#---------------------------#
# Run Gradient Forest
#---------------------------#
#---------------------------#
# set up cross-validation to get variance of diagnostics
#---------------------------#
# create a loop that takes each combination of calibration and validation data, train a model using the calibration data, 
# use that model to make predictions for the validation data, and then calculates kappa/acc by comparing the predictions to the observations 
#---------------------------#

#---------------------------#
# 10-fold cross validation 
#---------------------------#
nrows_RF <- nrow(forRF)
nfold <-10 # Set number of 'folds' for cross-validation
split <- sample(rep(1:nfold, length = nrows_RF), nrows_RF) # Split n into 10 sets (assign each sample to a group 1:10)
#Split the data into 10 groups of validation (10%) and calibration (90%). A site is put into the validation set if it was given that value in the the split. These cal/val groups are unique (ie no replacement)
resample <- lapply(1:nfold, function(x,spl) list(cal=which(spl!=x), val=which(spl==x)), spl=split) 

resultsFold = list() # This creates an empty vector to hold the AUC results.
for(fold in 1:nfold) { # Create a loop that runs through all folds
# Create the cal/val data sets for the current 'fold'
  cal1 = forRF[resample[[fold]]$cal,]
  val1 = forRF[resample[[fold]]$val,]
  
# Fit a model with the current calibration data set
  if(modelType=="cluster"){
      #RFmodelnew <- randomForest(cl~., data=cal1[,-1], importance=T, varUsed=T,ntree=10000)
      RFmodelnew <- randomForest(cl~., data=cal1, importance=T, varUsed=T,ntree=10000)
      observations1<-val1$cl                      #Observed cluster in the validation dataset 
      predictions1<-predict(RFmodelnew,val1[,-1]) #probability of predicted values for our testing dataset, based on our model
  } else {
      names(val1)[which(names(val1)==sp)]<-"species"
      names(cal1)[which(names(cal1)==sp)]<-"species"  
      RFmodelnew <- randomForest(species~., data=cal1, importance=T, varUsed=T,ntree=10000) 
      observations1<-val1$species             #Observations of the species in the validation dataset
      predictions1<-predict(RFmodelnew,val1)  #probability of predicted values for our testing dataset, based on our model
  }
  conf<-caret::confusionMatrix(observations1, predictions1) 
  resultsFold[[fold]]<- c(conf$overall["Kappa"],conf$overall["Accuracy"] )  # Attach kappa and accuracy to any previous content of 'results'
  }

meanFold<-data.frame(do.call("rbind",resultsFold))
meanFold<-data.frame(method=rep("tenfold",2),diagnostic=c("kappa","accuracy"), mean=c(mean(meanFold$Kappa),  mean(meanFold$Accuracy)), sd=c(sd(meanFold$Kappa),sd(meanFold$Accuracy)))

#---------------------------#
# Same cross-fold validation, but by holding back each survey as validation (to account for some level of auto-correlation in the cal/val sets)
#---------------------------#
if (!is.na(surveyField)){
surveys<-unique(forRF[,surveyField])

resultsSurv = c() 
for(i in 1:length(surveys)){ # Create a loop that runs through all surveys
# Create the cal/val data sets for the current 'fold'
	  cal1<-forRF[forRF[,surveyField]!=surveys[i],] #subset the input by holding back one survey (P1429) 
	  val1<-forRF[forRF[,surveyField]==surveys[i],]
	  
    # Fit a model with the current calibration data set
	if (modelType=="cluster"){
      RFmodelnew <- randomForest(cl~., data=cal1, importance=T, varUsed=T,ntree=10000) 
      observations1<-val1$cl 
      predictions1<-predict(RFmodelnew,val1[,-1]) 
  } else {
	    names(val1)[which(names(val1)==sp)]<-"species"
	    names(cal1)[which(names(cal1)==sp)]<-"species"  
  	  RFmodelnew <- randomForest(species~., data=cal1, importance=T, varUsed=T,ntree=10000) 
      observations1<-val1$species 
      predictions1<-predict(RFmodelnew,val1) 
  }
  conf<-caret::confusionMatrix(observations1, predictions1) #kappa is 0.1983
  resultsSurv[[i]] = c(conf$overall["Kappa"],conf$overall["Accuracy"] ) 
  }

meanSurv<-data.frame(do.call("rbind",resultsSurv))
meanSurv<-data.frame(method=rep("hold1Surv",2),diagnostic=c("kappa","accuracy"), mean=c(mean(meanSurv$Kappa),  mean(meanSurv$Accuracy)), sd=c(sd(meanSurv$Kappa),sd(meanSurv$Accuracy)))
}

#---------------------------#
# Run single model on full dataset to get variable importances and to use in prediction
#---------------------------#
if (modelType=="cluster"){
  RFmodel <- randomForest(cl~., data=forRF, importance=T, varUsed=T,ntree=10000)
} else {
  names(forRF)[which(names(forRF)==sp)]<-"species"  
  RFmodel <- randomForest(species~., data=forRF, importance=T, varUsed=T,ntree=10000)
}

# Set up model results for plot
biome_imp<-as.data.frame(RFmodel$importance)
biome_imp$param<-as.factor(rownames(biome_imp))
biome_imp<-within(biome_imp, param<-reorder(param, MeanDecreaseAccuracy))

biomeimpm<-melt(biome_imp, id="param")
biomeimpm<-biomeimpm[!biomeimpm$variable%in% c("MeanDecreaseAccuracy","MeanDecreaseGini"),]

if (modelType=="cluster"){
  plotdat<-biomeimpm
} else {
  plotdat<-biomeimpm[biomeimpm$variable==1,]
  plotdat<-within(plotdat, param<-reorder(param, value))
  }

plot1<-ggplot(plotdat, aes(x=value, y=param))+  geom_point(size=4, shape=20)+  
  facet_grid(.~variable)+  theme_bw()+theme(axis.text.x = element_text(angle = 45, hjust = 1))+ 
  theme(  panel.grid.major = element_blank(), panel.grid.minor = element_blank(), 
panel.background = element_blank())+ xlab("Mean Decrease in Accuaracy") +  ylab("") 

png(paste0(outdir, "/variableImportance_", figLabel,".png"), height=500, width=1200, res=100)
plot1
dev.off()

forRF$Substrate<-as.factor(forRF$Substrate)

#---------------------------#
# Predict and write output raster
#---------------------------#
# This may take a long time depending on the raster

if (predict=="yes"){
  if (modelType=="cluster"){
      RFmodelNoSurv <- randomForest(cl~., data=forRF[,!names(forRF)%in% c(surveyField)], importance=T, varUsed=T,ntree=10000) #Does not survey
    } else {
      RFmodelNoSurv <- randomForest(species~., data=forRF[,!names(forRF)%in% c(surveyField)], importance=T, varUsed=T,ntree=10000) #Does not survey
    }

map<-predict(ras,RFmodelNoSurv)
proj4string(map)<-CRS("+proj=aea +lat_1=50 +lat_2=58.5 +lat_0=45 +lon_0=-126 +x_0=1000000 +y_0=0 +datum=NAD83 +units=m +no_defs") #set coordinate reference system
writeRaster(map, paste0(outdir, "/randomForest_predicted_",figLabel,".tif"),filetype="GTiff" )

##add predictions to each record
matFullCl2<-matFullCl 
coordinates(matFullCl2)<-~x+y #tell R what the coordinates are

proj4string(matFullCl2)<-CRS("+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs") #set coordinate reference system
gridpoints_comdat<-spTransform(matFullCl2, CRS("+proj=aea +lat_1=50 +lat_2=58.5 +lat_0=45 +lon_0=-126 +x_0=1000000 +y_0=0 +datum=NAD83 +units=m +no_defs")) #project
matFullCl2_withPred<- data.frame(matFullCl2,extract(map, matFullCl2)) 

if (modelType=="cluster"){
    predata<-cbind.data.frame(matFullCl2_withPred$cl, matFullCl2_withPred$extract.map..matFullCl2.)
    names(predata)<-c("cluster","predicted")
    predata<-predata[predata$cluster%in% legendcluster$cl,]
  } else {
    predata<-cbind.data.frame(matFullCl2_withPred[,sp], matFullCl2_withPred$extract.map..matFullCl2.)
    names(predata)<-c("species","predicted")
}

#Calculate raster predictions
predata$predicted<-as.factor(predata$predicted)
predata[,1]<-as.factor(predata[,1])
confmap<-table(predata$predicted,predata[,1])
accmap<-sum(diag(confmap))/sum(confmap)

#---------------------------#
# Write all diagnostics
#---------------------------#
if(!is.na(surveyField)){
  diagnostics<-rbind.data.frame(meanFold, meanSurv)
} else (diagnostics<-rbind.data.frame(meanFold))}

if (predict=="yes"){
  diagnostics$method<-as.character(diagnostics$method)
  diagnostics<-rbind.data.frame(diagnostics, c("prediction","accuracy",accmap, NA))
}
write.csv(diagnostics,  paste0(outdir,"diagnostics.csv"),row.names=F)


#---------------------------#
# Make plots of each cluster's environment based on input data (not prediction)
#---------------------------#

if(modelType=="cluster"){
  forPlot<-matFullCl[, c("cl", env, idField)]
  forPlot.melt<-melt(forPlot, id.vars=c(idField, "cl"))
  forPlot.melt<-forPlot.melt[forPlot.melt$cl%in% legendcluster$cl,]
  forPlot.melt$cl<-as.factor(as.character(forPlot.melt$cl))
  forPlot.melt<-merge(forPlot.melt, legendcluster, by="cl")
} else {
  forPlot<-matFullCl[, c(sp, env, idField)]
  forPlot.melt<-melt(forPlot, id.vars=c(idField, sp))
  forPlot.melt[,sp]<-as.factor(as.character(forPlot.melt[,sp]))  
  }

# #Optional - truncate these variables
# forPlot.melt$value[forPlot.melt$variable=="dist_rock" & forPlot.melt$value>100]<-NA
# forPlot.melt$value[forPlot.melt$variable=="dist_sand" & forPlot.melt$value>100]<-NA
# forPlot.melt$value[forPlot.melt$variable=="dist_mud" & forPlot.melt$value>100]<-NA

png(paste0(outdir, "/allVars_",figLabel, ".png"), res=300, height=28, width=40, units="cm")
  if(modelType=="cluster"){
    ggplot(forPlot.melt, aes(x=cl, y=value,fill=cl))+geom_boxplot()+geom_violin(fill=NA)+facet_wrap(.~variable, scales = "free")+theme_bw()  +
      scale_fill_manual(values=unique(as.character(forPlot.melt$assigned)))
  } else {
    ggplot(forPlot.melt, aes(x=sp, y=value))+geom_boxplot()+facet_wrap(.~variable, scales = "free")+theme_bw()
  }
dev.off()


# # Extra - try RDA to visualize environmental differences in clusters
# forRDA<-matFullCl[matFullCl$cl%in% legendcluster$cl,]
# try<-rda(forRDA[,env] )
# biplot(try,display = c("sites"),type = c("points"), choices=c(1,3))
# ordihull(try,group = forRDA$cl, col=c("red","darkgreen","purple","blue","darkgrey","orange","black"), choices=c(1,3))
# plot(envfit(try, forRDA[,env], choices=c(1,3)))
# 
# envf<-envfit(try~CorDepthM+ Slope+Wood_Bark+Bedrock__s+Bedrock_wi+Boulders+Cobble+Gravel+Pea_Gravel+ Sand +Mud+Crushed_Sh+Whole_Chun, matFullCl, choices=c(1:3))
# ordirgl (try, display="sites",  col=matFullCl$cl, choices=c(1:3), envfit=envf)
# ordihull(trySp,group = matFullCl$cl, col=c("red","darkgreen","purple","blue","darkgrey","orange", "black","green"))
